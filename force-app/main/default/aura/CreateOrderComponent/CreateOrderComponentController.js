({
    doInit : function(component, event, helper) {
        helper.mostOrderedProducts(component, event, helper);
    },
    handleSubmit : function(component, event, helper) {
        helper.createOrder(component, event, helper);
    },
    handleExit : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    addProduct : function(component, event, helper) {
        var currentProduct = component.get('v.currentProduct');
        var orderedProducts = component.get('v.orderedProducts');
        for(let i = 0; i < orderedProducts.length; i++) {
            if(currentProduct == orderedProducts[i].Id) {
                return;
            }
        }
        if(currentProduct != null) {
            helper.loadProduct(component, event, helper);
        }
    },
    removeProduct : function(component, event, helper) { 
        
        var productToDelete = event.getSource().get("v.alternativeText");

        var orderedProducts = component.get('v.orderedProducts');
        for(let i = 0; i < orderedProducts.length; i++) {
            if(productToDelete.Id == orderedProducts[i].Id) {
                orderedProducts.splice(i, 1);
                break;
            }
        }
        component.set('v.orderedProducts', orderedProducts);
    }
})