({
    mostOrderedProducts : function(component, event, helper) {
        var action = component.get("c.get5moreRelevantProducts");
        action.setParams({
            'contactId': ''+component.get('v.recordId')+''
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    component.set("v.mostOrderedProducts", result);
                } else {
                    alert('error');
                }
        });
        $A.enqueueAction(action);
    },
    createOrder : function(component, event, helper) {

        var productIds = [];
        var productIdsList = component.get('v.orderedProducts');
        for(let i = 0; i < productIdsList.length; i++) {
            productIds.push(productIdsList[i].Id);
        }

        var action = component.get("c.createOrder");
        action.setParams({
            'contactId': ''+component.get('v.recordId')+'',
            'productIdsJSON': JSON.stringify(productIds)
        });

        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                helper.showToast('Success', 'Order was created', 'success');
                $A.get("e.force:closeQuickAction").fire();
            } else {
                helper.showToast('Error', 'error', 'error');
            }
        });
        $A.enqueueAction(action);
    },
    loadProduct : function(component, event, helper) {
        var action = component.get("c.getProduct");
        action.setParams({
            'product2Id': ''+component.get('v.currentProduct')+''
        });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();

                var orderedProducts = component.get('v.orderedProducts');
                orderedProducts.push(result);
                component.set('v.orderedProducts', orderedProducts);

                helper.showToast('Success', 'Product loaded', 'success');
                return result;
            } else {
                helper.showToast('Error', 'error', 'error');
                return null;
            }
        });
        $A.enqueueAction(action);
    },
    removeProductFromCart : function(component, event, helper, productToDelete) {
        
    },
    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    }
})