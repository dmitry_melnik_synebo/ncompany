public with sharing class ContactManager {


    // UPDATE
    public static void updateContactsInFakeStore(List<Contact> contactList) {
        for(Contact contact_i : contactList) {
            ContactWrapper respWrap = new ContactWrapper();
            
            Name name = new Name();
            name.FirstName = contact_i.FirstName;
            name.LastName = contact_i.LastName;
            
            Geo geo = new Geo();
            geo.lat = String.valueOf(contact_i.MailingLatitude);
            geo.longitude = String.valueOf(contact_i.MailingLongitude);
            
            Adress address = new Adress();
            address.city = contact_i.MailingCity;
            address.street = contact_i.MailingStreet;
            address.num = 1;
            address.zipcode = contact_i.MailingPostalCode;
            address.geolocation = geo;
            
            respWrap.email = contact_i.Email;
            respWrap.username = contact_i.Name;
            respWrap.password = contact_i.Password__c;
            respWrap.name = name;
            respWrap.address = address;
            respWrap.phone = contact_i.Phone;
            
            putContactsCallout(contact_i.ExternalId__c, JSON.serialize(respWrap));
        }
    }

    @future (callout=true)
    public static void putContactsCallout(String exId, String contactJSON) {
        Http http = new Http();
		HttpRequest request = new HttpRequest();
        request.setEndpoint('https://fakestoreapi.com/users/' + exId);
		request.setMethod('PUT');
		request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody(contactJSON);
        HttpResponse response = http.send(request);
        if (response != null && response.getStatus() == 'OK') {
            system.debug('contacts updated successfully');
        } else {
            system.debug('contacts updated UNsuccessfully');
            system.debug(response.getStatus());
            system.debug(response.getStatusCode());
        }
    }

    // ---------------------------------------------------------------------------------------

    // Load Contacts from Fake Store
    public static Boolean loadAllContacts() {
        HttpResponse response = getAllContactsCallout();
        if (response.getStatusCode() == 200) {
            List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());

            List<Contact> contactsToInsert = new List<Contact>();
            for(Object object_i : results) {
                ContactWrapper response_i = ContactWrapper.parse(JSON.serialize(object_i));

                Contact user = new Contact(
                    ExternalId__c = response_i.id,
                    Email = response_i.email,
                    //Name = response_i.username,
                    Password__c = response_i.password,
                    FirstName = response_i.name.firstname,
                    LastName = response_i.name.lastname,
                    MailingCity = response_i.address.city,
                    MailingStreet = response_i.address.street,
                    // response_i.address.num
                    MailingPostalCode = response_i.address.zipcode,
                    MailingLatitude = response_i.address.geolocation.lat != null ? Decimal.valueOf(response_i.address.geolocation.lat) : 1,
                    MailingLongitude = response_i.address.geolocation.longitude != null ? Decimal.valueOf(response_i.address.geolocation.longitude) : 1,
                    Phone = response_i.phone
                );
                contactsToInsert.add(user);
            }
            insert contactsToInsert;
            return true;
        }
        return false;
    }

    public static HttpResponse getAllContactsCallout() {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://fakestoreapi.com/users');
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        return response;
    }
    
    public static ContactWrapper parse(String jsonString){
        return (ContactWrapper)JSON.deserialize(jsonString, ContactWrapper.class);
    }

    public class ContactWrapper {
        public String id;
        public String email;
        public String username;
        public String password;
        public Name name;
        public Adress address;
        public String phone;
    }
    
    public class Name {
    	public String firstname;
        public String lastname;
    }
    
    public class Adress {
    	public String city;
        public String street;
        public Integer num;
        public String zipcode;
        public Geo geolocation;
    }
    
    public class Geo {
        public String lat;
        public String longitude;
    }
}