public with sharing class CreateOrderController {
    
    @AuraEnabled
    public static List<Product2> getAllProducts() {
        return [SELECT Id, Name, Price__c FROM Product2];
    }

    @AuraEnabled
    public static Product2 getProduct(Id product2Id) {
        Product2 product = [SELECT Id, Name FROM Product2 WHERE Id = :product2Id LIMIT 1];
        return product;
    }


    @AuraEnabled
    public static void createOrder(Id contactId, String productIdsJSON) {
        system.debug('productIdsJSON: '+productIdsJSON);
        List<String> productIdList = (List<String>)System.JSON.deserialize(productIdsJSON, List<String>.class);
        system.debug('productId: '+productIdList);
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Fake Store' LIMIT 1];
        Pricebook2 priceBook = [SELECT Id, Name, IsStandard FROM Pricebook2 WHERE IsStandard = TRUE LIMIT 1];

        Order order = new Order(
            AccountId = acc.id,
            EffectiveDate = Date.today(),
            Status = 'Draft',
            Contact__c = contactId,
            Pricebook2Id = priceBook.id
            // set order amount
        );
        insert order;
        
        List<Product2> productList = [SELECT Id, Name, Price__c FROM Product2 WHERE Id IN :productIdList];
        List<OrderItem> orderItemsToInsert = new List<OrderItem>();
        
        List<PricebookEntry> pbeList = [SELECT Id, Name,Product2Id FROM PricebookEntry WHERE Product2Id IN :productList];
        
        system.debug('productList: ' + productList);
        
        Map<Id, PricebookEntry> productIdToPbe = new Map<Id, PricebookEntry>();
        for(PricebookEntry pbe_i : pbeList) {
            productIdToPbe.put(pbe_i.Product2Id, pbe_i);
        }
        
        Decimal orderAmount = 0;
        for(Product2 product_i : productList) {
			if(product_i.Price__c != null) {
                orderAmount+=product_i.Price__c;
            }
            OrderItem orderItem = new OrderItem(
                OrderId = order.id,
                Product2Id = product_i.Id,
                Quantity = 1,
                PricebookEntryId = productIdToPbe.get(product_i.id).id,
                UnitPrice = product_i.Price__c
            );
            
            orderItemsToInsert.add(orderItem); 
        }
        insert orderItemsToInsert;

        order.Amount__c = orderAmount;
        update order;

        OrderManager.createOrderInFakeStore(order, orderItemsToInsert);
    }


    // @AuraEnabled
    // public static void createOrder(Id contactId) {
    //     Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Fake Store' LIMIT 1];
    //     Order order = new Order(
    //         AccountId = acc.id,
    //         EffectiveDate = Date.today(),
    //         Status = 'Draft',
    //         Contact__c = contactId
    //         // set order amount
    //     );
    //     insert order;
    // }

    @AuraEnabled
    public static List<Product2> get5moreRelevantProducts(Id contactId) {
        // TODO
        List<Product2> mostOrderedProducts = new List<Product2>();
        try {

        
        List<Order> orderList = [SELECT Id, Name, Contact__c FROM Order WHERE Contact__c = :contactId];
        List<OrderItem> orderItemList = [SELECT Id, OrderId, Product2Id FROM OrderItem WHERE OrderId IN :orderList];
        system.debug('orderItemList: ' + orderItemList);
        Map<Id, Integer> productQuantityMap = new Map<Id, Integer>();
        for(OrderItem orderItem_i : orderItemList) {
            system.debug('Product2Id: ' + orderItem_i.Product2Id);
            if(!productQuantityMap.containsKey(orderItem_i.Product2Id)) {
                productQuantityMap.put(orderItem_i.Product2Id, 1);
            }
            else {
                productQuantityMap.put(orderItem_i.Product2Id, productQuantityMap.get(orderItem_i.Product2Id) + 1);
            }
        }
        system.debug('productQuantityMap: '+productQuantityMap);
        Set<Id> productIds = productQuantityMap.keySet();
        Map<Id,Product2> productMap = new Map<Id,Product2>([SELECT Id, Name, Price__c FROM Product2 WHERE Id IN :productIds]);

        List<ProductWrapper> productWrappers = new List<ProductWrapper>();
        for(Id product_id : productQuantityMap.keySet()) {
            productWrappers.add(new ProductWrapper(productMap.get(product_id), productQuantityMap.get(product_id)));
        }
        productWrappers.sort();
        system.debug('productWrappers: ' + productWrappers);

        
        Integer productCount = productWrappers.size() > 5 ? 5 : productWrappers.size();
        for(Integer i = 0; i < productCount; i++) {
            mostOrderedProducts.add(productWrappers[i].product);
        }
        }
        catch(Exception e) {
            system.debug('error: ' + e.getMessage() + ' - ' + e.getLineNumber());
        }
        
        return mostOrderedProducts;
        // test
        //return [SELECT Id, Name FROM Product2 LIMIT 5];
    }

    public class ProductWrapper implements Comparable {
        @AuraEnabled
        public Product2 product;
        @AuraEnabled
        public Integer quantity;

        public ProductWrapper(Product2 product, Integer quantity) {
            this.product = product;
            this.quantity = quantity;
        }

        public Integer compareTo(Object compareTo) {
            ProductWrapper compareToOppy = (ProductWrapper)compareTo;
            Integer returnValue = 0;
            if (quantity > compareToOppy.quantity) {
                returnValue = -1;
            } else if (quantity < compareToOppy.quantity) {
                returnValue = 1;
            } 
            return returnValue; 
        }
    }
}