@IsTest
public inherited sharing class CreateOrderControllerTest {
    
    @IsTest
    public static void createOrderTest() {
        Account acc = new Account(
        	Name = 'Fake Store'
        );
        insert acc;
        
        Pricebook2 pb = new Pricebook2(
            Id  = Test.getStandardPricebookId(),
            IsActive = true
        );
        update pb;
        
		Contact contact = new Contact(
        	LastName = 'Test'
        );
        insert contact;
        Product2 pr1 = new Product2(
        	Name = 'test pr1',
            Price__c = 3
        );
        insert pr1;
        Product2 pr2 = new Product2(
        	Name = 'test pr2',
            Price__c = 3
        );
        insert pr2;
        List<Id> productList = new List<Id>{pr1.id, pr2.id};
        
        CreateOrderController.createOrder(contact.id, productList);
        
        List<Order> orderResult = [SELECT id, Name FROM order WHERE Contact__c = :contact.id LIMIT 1];
        System.assert(orderResult != null);
        
    }
}