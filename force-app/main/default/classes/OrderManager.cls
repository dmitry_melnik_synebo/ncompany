public with sharing class OrderManager {

    
    // CREATE (one order)
    public static void createOrderInFakeStore(Order order, List<OrderItem> orderItemList) {

        Map<Id, String> productIdToExternalId = new Map<Id, String>();

        Set<Id> productIdSet = new Set<Id>();
        for(OrderItem orderItem_i : orderItemList) {
            productIdSet.add(orderItem_i.Product2Id);
        }
        List<Product2> productList = [SELECT Id, name, ExternalId FROM Product2 WHERE Id IN :productIdSet];
        for(Product2 product_i : productList) {
            productIdToExternalId.put(product_i.id, product_i.ExternalId);
        }

        List<ProductWrapper> products = new List<ProductWrapper>();

        for(OrderItem orderItem_i : orderItemList) {
            ProductWrapper productWrap = new ProductWrapper();
            productWrap.quantity = Integer.valueOf(orderItem_i.Quantity);
            productWrap.productId = productIdToExternalId.get(orderItem_i.product2Id);
            products.add(productWrap);
        }

        ResponseWrapper respWrap = new ResponseWrapper();
        respWrap.userId = order.Contact__c;
        respWrap.dat = order.EffectiveDate;
        respWrap.products = products;
        String orderJSON = JSON.serialize(respWrap);
        system.debug('orderJSON: ' + orderJSON);
        postOrders(orderJSON);
    }

    @future (callout=true)
    public static void postOrders(String ordersJson) {
        Http http = new Http();
		HttpRequest request = new HttpRequest();
        request.setEndpoint('https://fakestoreapi.com/carts');
		request.setMethod('POST');
		request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody(ordersJson);
        HttpResponse response = http.send(request);
        if (response != null && response.getStatus() == 'OK') {
            system.debug('orders created successfully');
        } else {
            system.debug('orders created UNsuccessfully');
            system.debug(response.getStatus());
            system.debug(response.getStatusCode());
        }
    }

    
    // Load Order from Fake Store
    public static Boolean loadAllOrdersFromFakeStore() {
        HttpResponse response = getAllOrdersCallout();

        if (response.getStatusCode() == 200) {
            List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());

            List<Order> ordersToInsert = new List<Order>();
            
            Set<String> contactExIds = new Set<String>();
            Set<String> productExIds = new Set<String>();
            
            List<ResponseWrapper> responseWrapList = new List<ResponseWrapper>();
            for(Object object_i : results) {
				ResponseWrapper response_i = ResponseWrapper.parse(JSON.serialize(object_i));
                responseWrapList.add(response_i);
                contactExIds.add(response_i.userId);
                for(ProductWrapper productWrap_i : response_i.products) {
                    productExIds.add(productWrap_i.productId);                   
                }
            }
            List<Contact> contactList = [SELECT Id, Name, ExternalId__c FROM Contact WHERE ExternalId__c IN :contactExIds];
            Map<String, Contact> exIdToContactMap = new Map<String, Contact>();
            for(Contact contact_i : contactList) {
                exIdToContactMap.put(contact_i.ExternalId__c, contact_i);
            }
            List<Product2> productList = [SELECT Id, ExternalId, Price__c FROM Product2 WHERE ExternalId IN :productExIds];
            Map<String, Product2> exIdToProductMap = new Map<String, Product2>();
            for(Product2 product_i : productList) {
                exIdToProductMap.put(product_i.ExternalId, product_i);
            }
            List<PricebookEntry> pbeList = [SELECT Id,Product2Id FROM PricebookEntry WHERE Product2Id IN :productList];
            Map<Id, PricebookEntry> productIdToPricebookEntryMap = new Map<Id, PricebookEntry>();
            for(PricebookEntry pbe : pbeList) {
				productIdToPricebookEntryMap.put(pbe.Product2Id, pbe);
            }
            
            Pricebook2 pb = [select Id, IsActive from PriceBook2 where IsStandard=True];
            for(ResponseWrapper respWrap_i : responseWrapList) {
				Order order = new Order(
                    ExternalId__c = respWrap_i.id,
					EffectiveDate = respWrap_i.dat != null ? respWrap_i.dat : Date.today(),
                    Contact__c = exIdToContactMap.get(respWrap_i.userId).id,
                    Status = 'Draft',
                    OwnerId = UserInfo.getUserId(),
                    AccountId = '0010900000AyivvAAB',
                    Pricebook2Id = pb.id
                );
                ordersToInsert.add(order);
            }
            insert ordersToInsert;
            
            Map<String, Order> exIdToOrderMap = new Map<String, Order>();
            for(Order order_i : ordersToInsert) {
                exIdToOrderMap.put(order_i.ExternalId__c, order_i);
            }

			List<OrderItem> orderItemsToInsert = new List<OrderItem>();            
            for(ResponseWrapper respWrap_i : responseWrapList) {
            	for(ProductWrapper productWrap_i : respWrap_i.products) {
               		OrderItem orderItem = new OrderItem(
               			OrderId = exIdToOrderMap.get(respWrap_i.id).id,
                        Product2Id = exIdToProductMap.get(productWrap_i.productId).id,
                        Quantity = productWrap_i.quantity,
                        PricebookEntryId = productIdToPricebookEntryMap.get(exIdToProductMap.get(productWrap_i.productId).id).id,
                        UnitPrice = exIdToProductMap.get(productWrap_i.productId).Price__c
               		);   
                    orderItemsToInsert.add(orderItem);
               	} 
            }
            insert orderItemsToInsert;
            return true;
        }
        return false;
    }
    
    public static HttpResponse getAllOrdersCallout() {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://fakestoreapi.com/carts');
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        return response;
    }

    public static ResponseWrapper parse(String jsonString){
        return (ResponseWrapper)JSON.deserialize(jsonString, ResponseWrapper.class);
    }

    public class ResponseWrapper {
        public String id;
        public String userId;
        public Date dat;
        public List<ProductWrapper> products;
    }

    public class ProductWrapper {
        public String productId;
        public Integer quantity;
    }
}