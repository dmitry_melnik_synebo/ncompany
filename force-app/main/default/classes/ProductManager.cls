public with sharing class ProductManager implements Schedulable {
    
    public void execute(SchedulableContext SC) {
    	 updateProductPriceFromFakeStore();
    }
    
    
    // Update products price from Fake Store
    public static void updateProductPriceFromFakeStore() {
        List<Product2> productList = [SELECT Id, ExternalId, Price__c FROM Product2 WHERE ExternalId != NULL];
        List<Product2> productsToUpdate = new List<Product2>();
        Map<String, ProductWrapper> exIdToProductWrapperMap = new Map<String, ProductWrapper>();
        
        HttpResponse response = getAllProductsCallout();
        if (response.getStatusCode() == 200) {
            List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());
            for(Object object_i : results) {
                ProductWrapper response_i = ProductWrapper.parse(JSON.serialize(object_i));
                exIdToProductWrapperMap.put(response_i.id, response_i);
            }
        }
        for(Product2 product_i : productList) {
            if(product_i.Price__c != Decimal.valueOf(exIdToProductWrapperMap.get(product_i.ExternalId).price)) {
                product_i.Price__c = Decimal.valueOf(exIdToProductWrapperMap.get(product_i.ExternalId).price);
                productsToUpdate.add(product_i);
            }
        }
        update productsToUpdate;
    }
    
    
    // CREATE
    public static void createProductsInFakeStore(List<Product2> productList) {
        List<ProductWrapper> productToCreate = new List<ProductWrapper>();
        for(Product2 product_i : productList) {
			ProductWrapper respWrap = new ProductWrapper();
            respWrap.title = product_i.Name;
            respWrap.price = String.valueOf(product_i.Price__c);
            respWrap.category = product_i.Category__c;
            respWrap.description = product_i.Description;
            respWrap.image = product_i.Image__c;
            productToCreate.add(respWrap);
        }
        String products = JSON.serialize(productToCreate);
        postProductsCallout(products);
    }
    
    @future (callout=true)
    public static void postProductsCallout(String productsJSON) {
        Http http = new Http();
		HttpRequest request = new HttpRequest();
        request.setEndpoint('https://fakestoreapi.com/products');
		request.setMethod('POST');
		request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody(productsJSON);
        HttpResponse response = http.send(request);
        if (response != null && response.getStatus() == 'OK') {
            system.debug('adding new products successful');
        } 
        else {
             system.debug('adding new products UNsuccessful');
        }
    }
    
    // UPDATE
    public static void updateProductsInFakeStore(List<Product2> productList) {
        for(Product2 product_i : productList) {
			ProductWrapper respWrap = new ProductWrapper();
            respWrap.title = product_i.Name;
            respWrap.price = String.valueOf(product_i.Price__c);
            respWrap.category = product_i.Category__c;
            respWrap.description = product_i.Description;
            respWrap.image = product_i.Image__c;
            
            putProductCallout(product_i.ExternalId, JSON.serialize(respWrap));
        }
    }
    
    @future (callout=true)
    public static void putProductCallout(String exId, String productsJSON) {
        Http http = new Http();
		HttpRequest request = new HttpRequest();
        request.setEndpoint('https://fakestoreapi.com/products/' + exId);
		request.setMethod('PUT');
		request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody(productsJSON);
        HttpResponse response = http.send(request);
        if (response != null && response.getStatus() == 'OK') {
            system.debug('updated successful');
        } else {
            system.debug('updated UNsuccessful');
        }
    }
    
    // DELETE
    public static void deleteProductsFromFakeStore(List<Product2> productList) {
        for(Product2 product_i : productList) {
            deleteProductCallout(product_i.ExternalId);
        }
    }
    
    @future (callout=true)
    public static void deleteProductCallout(String exId) {
        Http http = new Http();
		HttpRequest request = new HttpRequest();
        request.setEndpoint('https://fakestoreapi.com/products/' + exId);
		request.setMethod('DELETE');
        HttpResponse response = Test.isRunningTest() ? null : http.send(request);
        if (response != null && response.getStatus() == 'OK') {
            system.debug('delete success');
        } else {
            system.debug('delete UNsuccess');
        }
    }


    // Load Products from Fake Store
    public static Boolean loadAllProductsFromFakeStore() {
        HttpResponse response = getAllProductsCallout();

        if (response.getStatusCode() == 200) {
            List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());
            Pricebook2 pb = [select Id, IsActive from PriceBook2 where IsStandard=True];

            List<Product2> productsToInsert = new List<Product2>();
            for(Object object_i : results) {
                ProductWrapper response_i = ProductWrapper.parse(JSON.serialize(object_i));

                Product2 product = new Product2(
                    ExternalId = response_i.id,
                    Name = response_i.title,
                    Price__c = Decimal.valueOf(response_i.price),
                    Category__c = response_i.category,
                    Description = response_i.description,
                    Image__c = response_i.image
                );
                productsToInsert.add(product);
            }
            insert productsToInsert;
            
            List<PricebookEntry> pbeToInsert = new List<PricebookEntry>();
            for(Product2 product_i : productsToInsert) {
                PricebookEntry pbe = new PricebookEntry (
                    Pricebook2Id=pb.id, 
                    Product2Id=product_i.id, 
                    IsActive=true, 
                    UnitPrice=product_i.Price__c
                );
                pbeToInsert.add(pbe);
            }
            insert pbeToInsert;
            
            return true;
        }
        return false;
    }
    
    public static HttpResponse getAllProductsCallout() {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://fakestoreapi.com/products');
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        return response;
    }

    public static ProductWrapper parse(String jsonString){
        return (ProductWrapper)JSON.deserialize(jsonString, ProductWrapper.class);
    }

    public class ProductWrapper {
        public String id;
        public String title;
        public String price;
        public String category;
        public String description;
        public String image;
    }
}