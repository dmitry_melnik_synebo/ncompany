trigger ProductTrigger on Product2 (before insert, after insert, after update, after delete) {
    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            ProductManager.createProductsInFakeStore(Trigger.new);
        }
        if(Trigger.isUpdate) {
            ProductManager.updateProductsInFakeStore(Trigger.new);
        }
        if(Trigger.isDelete) {
            ProductManager.deleteProductsFromFakeStore(Trigger.old);
        }
    }
}